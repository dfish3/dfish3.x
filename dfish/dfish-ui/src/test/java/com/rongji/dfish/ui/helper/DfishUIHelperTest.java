package com.rongji.dfish.ui.helper;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({FlexGridTest.class, TabPanelTest.class,GridPanelTest.class,
	GridLayoutFormPanelTest.class})
public class DfishUIHelperTest {

}
