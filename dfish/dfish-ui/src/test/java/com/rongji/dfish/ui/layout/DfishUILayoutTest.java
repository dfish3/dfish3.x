package com.rongji.dfish.ui.layout;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({GridLayoutTest.class})
public class DfishUILayoutTest {

}
