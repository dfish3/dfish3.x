package com.rongji.dfish.ui.widget;

/**
 * TemplateTitle 用于弹出窗口模板中顶部标题栏目的部分
 * 
 * @author DFish Team
 * @deprecated 3.2改版后用DialogTitle代替
 */
@Deprecated
public class TemplateTitle extends DialogTitle{

	public TemplateTitle(String text) {
		super(text);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4531981977671625714L;


}
