package com.rongji.dfish.ui.widget;

/**
 * Description: 对话模板主体
 * Copyright:   Copyright © 2018
 * Company:     rongji
 * @author		YuLM
 * @version		1.0
 *
 * Modification History:
 * Date						Author			Version			Description
 * ------------------------------------------------------------------
 * 2018年5月4日 下午6:06:23		YuLM			1.0				1.0 Version
 * @deprecated 3.2改版后用PreloadBody代替
 */
@Deprecated
public class TemplateBody extends PreloadBody{

	private static final long serialVersionUID = 9096485147445138199L;


}
