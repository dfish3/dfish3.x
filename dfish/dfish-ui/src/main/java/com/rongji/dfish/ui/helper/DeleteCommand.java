package com.rongji.dfish.ui.helper;

import com.rongji.dfish.ui.command.RemoveCommand;

/**
 * 删除某个命令/解析器/面板/按钮
 * 其中target表示 命令/解析器/面板/按钮 的id
 */
@Deprecated
public class DeleteCommand extends RemoveCommand{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1335552751374441172L;
	
}
