package com.rongji.dfish.ui;

/**
 * 当使用DFish3.2的template功能时，将会使用at的写法。
 * 利用该接口和JAVA8的新特性，可以让代码更加简约。
 * 它本身没有什么的实际用途
 * @author DFish team
 *
 */
public interface AtExpression {
	String expr();
}
