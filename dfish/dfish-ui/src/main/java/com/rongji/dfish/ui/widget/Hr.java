
package com.rongji.dfish.ui.widget;

/**
 * Hr 为分割线 2.x的版本常用，在3.x版本被Toggle替换，
 * @author DFishTeam
 * @see Toggle
 * @deprecated replaced by Toggle
 */
@Deprecated
public class Hr extends Toggle{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 4265871960115805666L;

	/**
	 * 默认构造函数
	 */
	public Hr(){
		 setHr(true);
	 }
}
