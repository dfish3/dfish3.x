package com.rongji.dfish.ui.layout;

/**
 * 帧面板 
 * 现在更名为FrameLayout
 * @author DFish team
 * @deprecated 现在更名为FrameLayout
 */
@Deprecated
public class FilmLayout extends FrameLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8234599818088516966L;

	/**
	 * 构造函数
	 * @param id String唯一标识
	 */
	public FilmLayout(String id) {
		super(id);
	}

}
