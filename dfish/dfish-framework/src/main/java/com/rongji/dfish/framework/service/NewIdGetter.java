package com.rongji.dfish.framework.service;

@Deprecated
public interface NewIdGetter {

	String getNewId(String clzName, String idName, String initId);

}
