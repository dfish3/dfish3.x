package com.rongji.dfish.misc.docpreview.data;

public interface ParagraphElement {
    String TYPE_CHARACTER_RUN="r";
    String TYPE_DRAWING="d";
    String getType();
}
