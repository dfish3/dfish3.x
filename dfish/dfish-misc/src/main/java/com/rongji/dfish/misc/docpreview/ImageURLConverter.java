package com.rongji.dfish.misc.docpreview;

public interface ImageURLConverter {
    String getDownloadURL(String relativeURL);
}
