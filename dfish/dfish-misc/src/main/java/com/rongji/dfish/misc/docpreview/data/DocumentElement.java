package com.rongji.dfish.misc.docpreview.data;

public interface DocumentElement {
    String TYPE_PARAGRAPH="p";
    String TYPE_TABLE="t";
    String getType();
}
